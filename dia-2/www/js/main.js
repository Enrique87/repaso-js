// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EXERCISE: "contentsDuplicate" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// const nums = [1, 2, 3, 1, 5];

// function isRepeat(arrayNums) {
//     for (let i = 0; i < arrayNums.length; i++) {
//         for (let j = i + 1; j < arrayNums.length; j++) {      // Pongo j = i+1 xq así cuando i empieza en posición 0, j empieza en 1,
//             if (arrayNums[i] === arrayNums[j]) {              // De esta manera compruebo si en el array[i] y el array [j] hay alguno igual/duplicado.
//                 return true;
//             }
//         }
//     }

//     return false;
// };

// console.log(isRepeat(nums));

// console.log("");


// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EXERCISE: "maxConsecutiveOnes"  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// const nums1 = [1, 0, 1, 1, 1, 0, 1];

// const findMaxConsecutiveOnes = function (nums) {      // es lo mismo que poner: " function findMaxConsecutiveOnes (nums) { "

//     let repeatedOnes = 0;

//     let mayor = 0;

//     for (let i = 0; i < nums.length; i++) {

//         if (nums[i] === 1) {
//             repeatedOnes++;
//             if (repeatedOnes > mayor) {
//                 mayor = repeatedOnes;
//             }
//         }

//         if (nums[i] === 0) {
//             repeatedOnes = 0;
//         }
//     }
//     return mayor;
// };

// console.log(findMaxConsecutiveOnes(nums1));

// console.log("");

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EXERCISE: "moveZeroes" (at the end) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// const nums1 = [0, 1, 0, 3, 12];

// let moveZeroes = function (nums) {
//     for (let i = 0; i < nums.length; i++) {
//         if (nums[i] === 0) {
//             nums.splice(i, 1);                     // "splice" = Elimina elemento/s en una posición.
//             nums.push(0);                         //  "push" = Añade un elemento al final del array. 
//         }
//     }
//     return nums;
// };

// console.log(moveZeroes(nums1));


// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EXERCISE 1: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// 
// Sara y Laura juegan al baloncesto en diferentes equipos.En los
// últimos 3 partidos, el equipo de Sara anotó 89, 120 y 103 puntos,
// mientras que el equipo de Laura anotó 116, 94, y 123 puntos.
// 
// `1.` Calcula la media de puntos para cada equipo.
// 
// `2.` Muestra un mensaje que indique cuál de los dos equipos
//      tiene mejor puntuación media.Incluye en este mismo mensaje
//      la media de los dos equipos.
//  
//  `3.` María también juega en un equipo de baloncesto.Su equipo
//       anotó 97, 134 y 105 puntos respectivamente en los últimos
//       3 partidos.Repite los pasos 1 y 2 incorporando al equipo de María.

// const pointsSara = [89, 120, 103];
// const pointsLaura = [116, 94, 123];

// let sum = pointsSara.reduce((previous, current) => { return current + previous; }, 0); // pongo el 0 para que empieze desde el valor incial,sino empezaría en 1.
// let avg = sum / pointsSara.length;

// let sum1 = pointsLaura.reduce((previous, current) => current += previous);             // equivale a lo mismo que el caso anterior.
// let avg1 = sum1 / pointsLaura.length;

// console.log(avg);
// console.log(avg1);

// function compareTeams() {
//     if (avg > avg1) {
//         console.log(`La mejor media es la del equipo de Sara que tiene ${avg} puntos de media por los ${avg1} puntos de media de Laura`);
//     }
//     else { console.log(`La mejor media es la del equipo de Laura que tiene ${avg1} puntos de media por los ${avg} puntos de media de Sara`); }
// };

// compareTeams();

// console.log("");

// const pointsSara1 = [89, 120, 103];
// const pointsLaura1 = [116, 94, 123];
// const pointsMaria1 = [97, 134, 105];

// let sumSara = pointsSara1.reduce((previous, current) => current += previous);
// let avgSara = sumSara / pointsSara1.length;

// let sumLaura = pointsLaura1.reduce((previous, current) => current += previous);
// let avgLaura = sumLaura / pointsLaura1.length;

// let sumMaria = pointsMaria1.reduce((previous, current) => current += previous);
// let avgMaria = sumMaria / pointsMaria1.length;

// console.log(avgSara);
// console.log(avgLaura);
// console.log(avgMaria);

// function compareTeamsTotal() {
//     if (avgSara > avgLaura && avgMaria) {
//         console.log(`La mejor media es la del equipo de Sara que tiene ${avgSara} puntos de media por los ${avgLaura} puntos de media de Laura y
//         los ${avgMaria} puntos de media de María`);
//     } else if (avgMaria > avgSara && avgLaura) {
//         console.log(`La mejor media es la del equipo de Maria que tiene ${avgMaria} puntos de media por los ${avgSara} puntos de media de Sara
//     y los ${avgLaura} puntos de media de Laura`);
//     }
//     else {
//         console.log(`La mejor media es la del equipo de Laura que tiene ${avgLaura} puntos de media por los ${avgSara} puntos de media de Sara
//     y los ${avgMaria} puntos de media de María`);
//     }
// };

// compareTeamsTotal();

// console.log("");

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EXERCISE 2: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// Jorge y su familia han ido a comer a tres restaurantes distintos.
// La factura fue de 124€, 58€ y 268€ respectivamente.
//  
// Para calcular la propina que va a dejar al camarero, Jorge ha
// decidido crear un sistema de calculo(una función).Quiere
// dejar un 20 % de propina si la factura es menor que 50€, un 15 %
// si la factura está entre 50€ y 200€, y un 10 % si la factura es mayor que 200€.
//  
// Al final, Jorge tendrá dos arrays:
//  
// `Array 1` Contiene las propinas que ha dejado en cada uno de los tres restaurantes.
// `Array 2` Contiene el total de lo que ha pagado en cada uno de los restaurantes(sumando la propina).


// const totalRestaurants = [124, 58, 268];

// function calculatePropina(pagos) {
//     for (let i = 0; i < totalRestaurants.length; i++) {
//         if (pagos[i] < 50) {
//             pagos[i] = pagos[i] * 0.2;
//         }
//         if (pagos[i] > 50 && pagos[i] < 200) {
//             pagos[i] = pagos[i] * 0.15;
//         }
//         if (pagos[i] > 200) {
//             pagos[i] = pagos[i] * 0.1;
//         }
//     }
//     return pagos;
// }

// console.log(calculatePropina(totalRestaurants));

// // console.log("");

// function calculateTotal(factura) {
//     for (let i = 0; i < totalRestaurants.length; i++) {
//         if (factura[i] < 50) {
//             factura[i] = (factura[i] * 0.2) + factura[i];            // ?¿?¿?
//         }
//         if (factura[i] > 50 && factura[i] < 200) {
//             factura[i] = (factura[i] * 0.15) + factura[i];
//         }
//         if (factura[i] > 200) {
//             factura[i] = (factura[i] * 0.1) + factura[i];
//         }
//     }
//     return factura;
// }

// console.log(calculateTotal(totalRestaurants));


// SOLUCIÓN EN CLASE:

// const totalFacturas = [124, 58, 268];
// const propinas = [];
// const totalDeTotales = [];

// for (const factura of totalFacturas) {
//     if (factura < 50) {
//         propinas.push((20 / 100) * factura);
//     }
//     else if (factura >= 50 && factura <= 200) {
//         propinas.push((15 / 100) * factura);
//     }
//     else {
//         propinas.push((10 / 100) * factura);
//     }
// }
// console.log(propinas)
// propinas.map((value, index) => {
//     const total = value + totalFacturas[index];
//     totalDeTotales.push(total)
// })
// console.log(totalDeTotales);

// console.log("");

// let facturas = [124, 58, 268];

// let nuevoArray = [];
// function calculaPropinas(numbers) {
//     for (let i = 0; i < numbers.length; i++) {
//         if (numbers[i] < 50) {
//             nuevoArray.push(numbers[i] * 0.2);
//         } else if (numbers[i] > 200) {
//             nuevoArray.push(numbers[i] * 0.1);
//         } else {
//             nuevoArray.push(numbers[i] * 0.15);
//         }
//     }
//     return nuevoArray;
// }
// let propinas = calculaPropinas(facturas);
// console.log(propinas);

// const total = nuevoArray.map((value, index) => {
//     return value + facturas[index];
// });
// console.log(total);

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EXERCISE 3: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// Dado el siguiente array de números:
//  
// `nums = [100, 3, 4, 2, 10, 4, 1, 10]`
// 
// `1.` Recorre todo el array y muestra por consola cada uno de sus
//      elementos con la ayuda de un`for`, con la ayuda de un`map`
//      y con la ayuda de un`for...of`.
//  
//  `2.` Ordena el array de menor a mayor sin emplear`sort()`.
//   
//  `3.` Ordena el array de mayor a menor empleando`sort()`.


// const nums3 = [100, 3, 4, 2, 10, 4, 1, 10];

// const newNums = nums3.map((index) => []);
// console.log(newNums);

// for (const [i] of nums3.entries()) {
//     console.log(i);
// } 




// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EXERCISE 4: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// Crea una`arrow function` que reciba dos números por medio del
// `prompt`, reste ambos números, y nos devuelva el resultado. 
// 
// En caso de que el resultado sea negativo debe cambiarse a
// positivo.Este resultado se mostrará por medio de un`alert`.


// function restResult() {

//     let n1 = parseInt(prompt('Tell me one number'));
//     let n2 = parseInt(prompt('Tell me other number'));
//     if (n1 - n2 >= 0) {                                     // condición siempre que sea mayor o igual que 0
//         return n1 - n2;                                     // devuelve la resta de ambos.  
//     } else {
//         alert("Es negativo, por tanto debería ser");        // sino alertame de que es negativo
//         return (n1 - n2) * (-1);                            // y cambialo a positivo al multiplicar por "-1".  
//     }
// }
// console.log(restResult());

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< DIA 2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EXERCISE: 121 "Best time to buy and Sell Stock" >>>>>>>>>>>>>>>>>>>>>>>>>

// const prices1 = [7, 1, 5, 3, 6, 4];

// const maxProfit = function (prices) {

//     let mayorLucro = 0;

//     for (let i = 0; i < prices.length - 1; i++) {

//         for (let j = i + 1; j < prices.length; j++) {

//             const lucro = prices[j] - prices[i];

//             if (lucro > mayorLucro) {

//                 mayorLucro = Math.max(mayorLucro, lucro);

//             }
//         }
//     }
//     return mayorLucro;
// };

// console.log(maxProfit(prices1));

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EXERCISE: 657 "ROBOT RETURN TO ORIGIN" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// const judgeCircle = function (moves) {

//     let up = 0;
//     let down = 0;                                        // Fijamos las posiciones en 0 como punto de partida en el cuadrante donde se mueva el robot.
//     let left = 0;
//     let right = 0;

//     const splitMoves = moves.split('');                 // ?¿?¿

//     for (const move of splitMoves) {

//         switch (move) {                                 // Usamos "switch" para tratar los diferentes escenarios de "move".

//             case 'U':                                  // En caso de subir.
//                 up++;                                  // Incrementa en 1.
//                 break;                                 // Paramos la acción.

//             case 'D':                                
//                 down++;
//                 break;

//             case 'L':
//                 left++;
//                 break;

//             default:
//                 right++;

//         }
//     }

//     const ejeX = right - left;                      // Fijamos como constante que el ejeX será derecha menos izquierda
//     const ejeY = up - down;                         // Fijamos como constante que el ejeY será subir menos bajar.

//     if (ejeX === 0 && ejeY === 0) {                 // Para el caso de que ambos ejes sean 0, signficará que el robot vuelve a regresado a su posición inicial
//         return true;                                // por tanto devolverá verdadero.
//     }

//     return false;

// };

// console.log(judgeCircle("UD"));                    // Al marcar que subio y a la vez bajo, estará en la posición 0, por tanto, devolverá verdadero.

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<EXERCISE OF AMAZON'S INTERWIEV >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// class Robot {
//     rowsPosition = 0;
//     columnsPosition = 0;

//     constructor(space) {
//         this.space = space;
//     }
//     moveLeft() {
//         if (this.columnsPosition === 0) {
//             return false;
//         }
//         this.columnsPosition--;
//         return true;
//     }
//     moveRight() {
//         if (this.columnsPosition === this.space[this.rowsPosition].length - 1) {
//             return false;
//         }
//         this.columnsPosition++;
//         return true;
//     }
//     moveUp() {
//         if (this.rowsPosition === 0) {
//             return false;
//         }
//         this.rowsPosition--;
//         return true;
//     }
//     moveDown() {
//         if (this.rowsPosition === this.space.length - 1) {
//             return false;
//         }
//         this.rowsPosition++;
//         return true;
//     }
//     currentPosition() {
//         return this.space[this.rowsPosition][this.columnsPosition];
//     }
// }

// const mySpace = [
//     [1, 9, 5],
//     [7, 6, 3],
//     [6, 6, 8]
// ];

// console.log(mySpace[0].length);
// console.log(mySpace[1][2]);
// const myLittlePrettyRobot = new Robot(mySpace);

// console.log(myLittlePrettyRobot.currentPosition());

// myLittlePrettyRobot.moveRight();
// myLittlePrettyRobot.moveRight();
// myLittlePrettyRobot.moveRight();
// console.log(myLittlePrettyRobot.currentPosition());

// myLittlePrettyRobot.moveDown();
// myLittlePrettyRobot.moveDown();
// console.log(myLittlePrettyRobot.currentPosition());

// myLittlePrettyRobot.moveUp();
// console.log(myLittlePrettyRobot.currentPosition());

// myLittlePrettyRobot.moveLeft();
// myLittlePrettyRobot.moveLeft();
// myLittlePrettyRobot.moveUp();
// console.log(myLittlePrettyRobot.currentPosition());

// <<<<<<<<<<<<<<<<<<<<<<<<<<<>>> CLASS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><

// class Animal {
//     constructor(name, legs, sound) {
//         this.name = name;
//         this.legs = legs;
//         this.sound = sound;
//         this.color = 'red';            // si fuese número no necesitaría comillas, es xq es un string
//     }

//     static showColor() {
//         return `El animal es de color ${this.color}`;
//     }
// }

// console.log(Animal, showColor());

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// class Animal {
//     constructor(name, legs, sound) {
//         this.name = name;
//         this.legs = legs;
//         this.sound = sound;
//         this.color = 'red';            // si fuese número no necesitaría comillas, es xq es un string
//     }
//     elAnimalDice() {
//         return `Hola soy ${this.name} tengo ${this.legs} patas y digo ${this.sound}.`
//     }
// }

// //Instancio un animal
// const myAnimal = new Animal('Paco', 4, 'guau');

// console.log(myAnimal.elAnimalDice());

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// class Animal {
//     constructor(name, legs, sound) {
//         this.name = name;
//         this.legs = legs;
//         this.sound = sound;
//         this.color = 'red';            // si fuese número no necesitaría comillas, es xq es un string
//     }
//     elAnimalDice() {
//         return `Hola soy ${this.name} tengo ${this.legs} patas y digo ${this.sound}.`
//     }
// }

//   //Instancio un animal
// const myAnimal = new Animal('Paco', 4, 'guau');

// console.log(myAnimal.elAnimalDice());

// class Cat extends Animal {
//     constructor(name, legs, sound, whiser) {   // hay que incluir las que se extiende y si quieres más
//         super(name, legs, sound);              // en super hay que repetir las propiedades extendidas  
//         this.whiser = whiser;                  // hay que llamar a la nueva clase.
//     }
// };

// const newCat = new Cat('Carbón', 4, 'miauuuu', true);  // y tengo que poner true para añadir whiser.

// console.log(newCat.elAnimalDice());

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EXERCISE 5 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

